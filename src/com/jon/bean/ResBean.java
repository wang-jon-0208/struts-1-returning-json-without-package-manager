package com.jon.bean;

import java.io.Serializable;

public class ResBean implements Serializable {
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
