package com.jon.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JacksonUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private JacksonUtils() {}

    public static ObjectNode objSetToJsonNode(Object originalObj) {
        return objectMapper.convertValue(originalObj, ObjectNode.class);
    }
}
