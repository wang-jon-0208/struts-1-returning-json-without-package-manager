package com.jon.action;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jon.bean.ResBean;
import com.jon.form.HelloWorldForm;
import com.jon.util.JacksonUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldAction extends Action {
    @Override
    public ActionForward execute(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response
    ) throws IOException {
        HelloWorldForm hwForm = (HelloWorldForm) form;
        hwForm.setMessage("Hello World");
        ResBean resBean = new ResBean();
        resBean.setMsg("Hello World");

        ObjectNode objectNode = JacksonUtils.objSetToJsonNode(resBean);

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(objectNode);
        out.flush();

        return null;
    }
}
