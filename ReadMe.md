# Struts 1 Returning JSON Without Package Manager

## Ref

參考 https://dzone.com/tutorials/java/struts/struts-tutorial/struts-tutorial-using-eclipse-1.html

另外有Struts 1 with maven:https://matthung0807.blogspot.com/2021/01/eclipse-create-struts-1-project.html

https://www.codejava.net/frameworks/struts/struts2-beginner-tutorial-eclipse-tomcat-xml 此Struts 2照做失敗 會噴一堆錯

## Why do I use Struts 1 without package manager in 2021?

Good question, 公司專案需求...

## Steps

Dynamic Web Project 選這種會建立全手動處理的專案 lib也要自己處理

![145ee974e0f14285edfca6ac84cc3ba7.png](./_resources/a9a4de15ada343b58ddc8759cf70d379.png)

![219b3d62deddc15aa8b1677b1c102f9a.png](./_resources/edaf5e7f55d54d5b9db0a99ef0b3ae5f.png)

Target runtime跟Dynamic web module version比較新

其他照做

## IntelliJ Ultimate env

1. Project Structure

![59843e3c70d445658e36f91f20f4127a.png](./_resources/33a3a342788645a3b4ce850801d7d986.png)

只要有正確的web.xml就會自動有facet 正常建立專案就會有web.xml

照著configure-OK就會有facet

![89d6a74c4240a3d1d6f42d96319ef9ef.png](./_resources/89d6a74c4240a3d1d6f42d96319ef9ef.png)

或是自己增加

![56ddeb21451f3641076000aaaddad871.png](./_resources/56ddeb21451f3641076000aaaddad871.png)

點Edit

![b1185a2c4d05904c0bd4fb65bb7f3cb8.png](./_resources/b1185a2c4d05904c0bd4fb65bb7f3cb8.png)

就可以選擇專案已經有的web.xml的路徑

![77ce0c04b5fedf90e49cdeb9e4d06cdd.png](./_resources/77ce0c04b5fedf90e49cdeb9e4d06cdd.png)

Create Artifact

![369a4667b565e2b20de214877dde08ce.png](./_resources/1d36e31ea9ca4d5dbb3c3e9376f4566c.png)

Fix

![b3648acda827c08e4e93f8c30e4869bf.png](./_resources/7963a84661bb490d974fbc284c2f3824.png)

Add lib to the artifact

點OK關閉Project Structure

2. Add Config

![2666ef86970cb2bd904f01ec1f490715.png](./_resources/edc9759c821c48daaec4c51fb4f6e1c5.png)

Add Configuration

![27b58786564bdfeba3685508ebf4d273.png](./_resources/7358184784a344328763e232ea494c6d.png)

Tomcat-local

![e41a40ea9de89cba95880d108d2af4b0.png](./_resources/670cf79504c04122a092a16aff70d215.png)

Fix

![6b5c0da1950fe2f1f3c185672b922a0a.png](./_resources/f5158aa44a3a40b3a0b194b57e0b92ce.png)

修改Deployment tab的Application context name

OK

3. dev with debugging in IDE

![aa80b783a61ae26644f53263f8572a11.png](./_resources/8321dcfa47d742a6bda6bd2ad1835033.png)

開啟 http://localhost:8080/helloWorld 看結果

## Eclipse env

![1898a12b7a31d49b7168b6f3b75aa956.png](./_resources/62516f87abf0468ab14a7cfdd69d3311.png)

![c84ee4225173398a83ccf59d4278a868.png](./_resources/493cc793d6b644708f239f8591facc88.png)

![d4b9959e31f36b6a86b4a85bc7a534cb.png](./_resources/42cf0f332a554b67a867383b01445d44.png)




